#pragma once

#include "ofMain.h"
#include "ofxSvg.h"
#include "ofxGUI.h"

class testApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void mouseReleased(int x, int y, int button);

		void calibrate();
		void serialRX();
		void serialTX(int x, int y);

		ofSerial arduino;

		int windowWidth, windowHeight;

		//TX
		char commandTX[40];
		int commandStrSizeTX;

		//RX
		bool serialReady, serialSent;
		char bufferRX[40], bufferCharRX;
		int bufferSizeRX, bufferIndexRX;

        const char delimiterRX[1] = {','};
        const char* commandRX[5];
        char* strPointerRX;

        //Stepper struct
        struct stepper {
            ofVec2f vA;
            ofVec2f vB;
            ofVec2f vC;
            int position;
            int previousPosition;
            int travel;
            int speed;
        };

        stepper stepperA;
        stepper stepperB;

        int defaultSpeed;
        float scaleFactor;
        int centerPosition;

        //GUI
        int gridLines;
        ofxPanel gui;
        ofxButton buttonCalibrate;
        ofxToggle toggleDraw;
        ofxLabel positionA;
        ofxLabel positionB;
        ofxLabel distanceA;
        ofxLabel distanceB;
        ofxLabel status;

        ofTrueTypeFont myfont;

        //Draw variables

        ofxSVG SVG;

        vector<ofPolyline> polyline;
        vector<ofPoint> points;

        bool nextPath, finishDrawing;
        int pathIndex, pathSize, pointSize, pointIndex;

};
