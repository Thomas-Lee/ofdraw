#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup(){

windowWidth = ofGetWindowWidth();
windowHeight = ofGetWindowHeight();

arduino.listDevices();
arduino.setup(0, 9600);
arduino.flush(true,true);

scaleFactor = 4.4*4;

//Set default speed
defaultSpeed = 350;

//distance between gridlines
gridLines = 100;

centerPosition = sqrt(windowWidth/2*windowWidth/2 + windowHeight/2*windowHeight/2)*scaleFactor;

ofNoFill();

myfont.loadFont("arial.ttf", 12);

//Setup gui
ofxBaseGui::setDefaultWidth(250);
gui.setup("Menu", "Settings.xml", 500, 600);
gui.add(buttonCalibrate.setup("Reset"));
gui.add(toggleDraw.setup("Draw", false));
gui.add(positionA.setup("Position sent stepper A","NULL"));
gui.add(positionB.setup("Position sent stepper B","NULL"));
gui.add(distanceA.setup("Distance to go A","NULL"));
gui.add(distanceB.setup("Distance to go B","NULL"));
gui.add(status.setup("Status","NULL"));
buttonCalibrate.addListener(this, &testApp::calibrate);

//Load SVG from data folder
SVG.load("test.svg");
pathSize = SVG.getNumPath();
pathIndex = 0;
pointIndex = 0;

finishDrawing = false;
nextPath = false;
serialReady = true;
serialSent = false;

cout << "Number of paths loaded: "+ofToString(pathSize)+"\n";

//Get first path
polyline = SVG.getPathAt(pathIndex).getOutline();

//Get points of first polyline
points = polyline[0].getVertices();
pointSize = points.size();

cout << "Points of first path: "+ofToString(pointSize)+"\n";

}

//--------------------------------------------------------------
void testApp::update(){

    serialRX();

    if(!finishDrawing && !serialSent && serialReady && toggleDraw){

        serialTX(points.at(pointIndex).x, points.at(pointIndex).y);
        pointIndex++;

        if(pointIndex+1 > pointSize){
            pathIndex++;
            if (pathIndex+1 > pathSize){
                finishDrawing = true;
                pathIndex = 0;
                pointIndex = 0;

                polyline = SVG.getPathAt(pathIndex).getOutline();
                points = polyline[0].getVertices();
                serialTX(windowWidth/2,windowHeight/2);
                toggleDraw = false;
                cout << "Drawing finished";

            } else {
                cout << "New path \n";
                polyline = SVG.getPathAt(pathIndex).getOutline();
                points = polyline[0].getVertices();
                pointSize = points.size();
                pointIndex = 0;
            }
        }

        serialReady = false;
        serialSent = true;

        //ofSleepMillis(10);

        //arduino.flush(true,false);
    }

}

//--------------------------------------------------------------
void testApp::draw(){

    ofSetColor(150,150,150);

    //Draw grid
    for(int i=0; i<windowWidth/gridLines; i++){
        ofLine(i*gridLines,0,i*gridLines,windowHeight);
        ofLine(0,i*gridLines,windowWidth,i*gridLines);
    }

    ofFill();
    ofCircle(points.at(pointIndex).x, points.at(pointIndex).y, 20);
    ofNoFill();

    ofSetColor(255,0,0);

    //Draw import vector
    SVG.draw();

    // Draw 0,0,0
    ofCircle(windowWidth/2,windowHeight/2,4);


    //Draw circle at points
    for(int j=0; j<points.size(); j++){
       ofCircle(points.at(j), 10);
   }

    ofSetColor(255,255,255);

    myfont.drawString("X:"+ofToString(mouseX-windowWidth/2)+" Y:"+ofToString(mouseY-windowHeight/2), mouseX+10, mouseY+40);
    myfont.drawString("0,0,0",windowWidth/2+4,windowHeight/2+16);

    gui.draw();

}

void testApp::mouseReleased(int x, int y, int button){

    serialTX(x,y);

}

void testApp::calibrate(){

    commandStrSizeTX = sprintf (commandTX, "%c,%i,%i%c", 'R', 0, 0, '#');
    cout << "Buffer sent: "+ofToString(commandTX)+"\n";
    //cout << "Buffer size: "+ofToString(bufferSizeTX)+"\n";
    arduino.writeBytes((unsigned char*)commandTX, commandStrSizeTX); //Include terminating NULL char

    finishDrawing = false;
    pathIndex = 0;
    pointIndex = 0;

    polyline = SVG.getPathAt(pathIndex).getOutline();
    points = polyline[0].getVertices();

    positionA="0";
    positionB="0";

    stepperA.previousPosition = 0;
    stepperB.previousPosition = 0;

}

void testApp::serialRX() {

    while(arduino.available() > 0){
        bufferCharRX = arduino.readByte();

        if(bufferCharRX != '#'){
            bufferRX[bufferIndexRX] = bufferCharRX;
            bufferIndexRX++;
        } else {

            //Include terminating NULL in buffer end - strtok looks for it to stop
            bufferRX[bufferIndexRX] = '\0';

            //StrTok delimit buffer
            strPointerRX = strtok(bufferRX, delimiterRX);
            for(int i=0; i < 3; i++){ //Go sthrough 3 sections - command,command,command#
                commandRX[i] = strPointerRX;
                strPointerRX = strtok(NULL, delimiterRX);
            }

        status = ofToString(commandRX[0]);
        distanceA = ofToString(commandRX[1]);
        distanceB = ofToString(commandRX[2]);

        bufferIndexRX=0;

        switch(*commandRX[0]) {
        case 'R':
            if(serialSent){
            serialReady = false;
            } else {
            serialReady = true;
            }
            break;
        case 'M':
            if(serialSent){
            serialReady = false;
            serialSent = false;
            }
            break;
        default:
            break;
        }

        }
    }
  }

void testApp::serialTX(int x, int y){

    //Update stepperA
    stepperA.vA.set(x,0);
    stepperA.vB.set(0,y);
    stepperA.vC = stepperA.vA + stepperA.vB;
    stepperA.position = round(stepperA.vC.length()*scaleFactor-centerPosition);

    //Update stepperB
    stepperB.vA.set(x-windowWidth,0);
    stepperB.vB.set(0,y);
    stepperB.vC = stepperB.vA + stepperB.vB;
    stepperB.position = round(stepperB.vC.length()*scaleFactor-centerPosition);

    //Get distance to move from last move
    stepperA.travel = abs(stepperA.previousPosition-stepperA.position);
    stepperB.travel = abs(stepperB.previousPosition-stepperB.position);

    //Set speed
    if(stepperA.travel > stepperB.travel  && stepperB.travel != 0){
        stepperB.speed = ((float)stepperB.travel/(float)stepperA.travel)*defaultSpeed;
        stepperA.speed = defaultSpeed;
    } else if (stepperB.travel > stepperA.travel && stepperA.travel != 0){
        stepperA.speed = ((float)stepperA.travel/(float)stepperB.travel)*defaultSpeed;
        stepperB.speed = defaultSpeed;
    } else {
        stepperA.speed = defaultSpeed;
        stepperB.speed = defaultSpeed;
    }

    stepperA.previousPosition = stepperA.position;
    stepperB.previousPosition = stepperB.position;

    //Set label string
    positionA=ofToString(stepperA.position);
    positionB=ofToString(stepperB.position);

    //Create string to send
    commandStrSizeTX = sprintf (commandTX, "%c,%i,%i,%i,%i%c", 'D', stepperA.position, stepperA.speed, stepperB.position, stepperB.speed, '#');
    cout << "Buffer sent: "+ofToString(commandTX)+"\n";

    //Send buffer
    arduino.writeBytes((unsigned char*)commandTX, commandStrSizeTX); //Include terminating NULL char



}
